﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public class TextConsoleMenuItem : ConsoleMenuItem, IComparable<TextConsoleMenuItem>
    {
        private string text;

        public string Text
        {
            get { return text; }
            set { text = value;
            KeyCharacter = text.Length > 0 ? text[0] : '0';
            }
        }

        public TextConsoleMenuItem(string text, string header, ConsoleCommandBase command, string description = "")
            : this(text, header, description)
        {
            Command = command;
        }

        public TextConsoleMenuItem(string text, string header, string description)
            : base(header, description)
        {
            Text = text;
        }

        public TextConsoleMenuItem(string text, string header)
            :base(header)
        {
            Text = text;
        }

        public int CompareTo(TextConsoleMenuItem other)
        {
            return Text.CompareTo(other.Text);
        }

        public override int CompareTo(ConsoleMenuItem other)
        {
            return CompareTo(other as object);
        }

        public override int CompareTo(object obj)
        {
            if (obj is TextConsoleMenuItem)
                return CompareTo(obj as TextConsoleMenuItem);
            else if (obj is ConsoleMenuItem)
                return 1;
            else
                throw new ArgumentException("Argument is not a TextualMenuChoice", "obj");
        }

        public override string ToString()
        {
            return String.Format("  {0} - {1}", Text, Header);
        }
    }
}
