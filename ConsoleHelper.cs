﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public static class ConsoleHelper
    {
        public static EscapeKeyBehavior EscBahavior { get; set; }

        public static char ExitChar { get; set; }

        static ConsoleHelper()
        {
            EscBahavior = EscapeKeyBehavior.PressTwiceToExit;
            ExitChar = '0';
        }

        public static ConsoleUserRequest TryReadItem(ConsoleMenuItem root, out ConsoleMenuItem readItem)
        {
            readItem = null;
            while (true)
            {
                var key = System.Console.ReadKey(true);

                if (key.Key == ConsoleKey.Escape)
                {
                    if (EscBahavior == EscapeKeyBehavior.PressOnceToExit)
                        return ConsoleUserRequest.Exit;
                    else if (EscBahavior == EscapeKeyBehavior.PressTwiceToExit)
                    {
                        if (System.Console.ReadKey(true).Key == ConsoleKey.Escape)
                            return ConsoleUserRequest.Exit;
                    }
                }
                else if (key.Key == ConsoleKey.Insert)
                {
                    ConsoleTheme.Cyan100.Apply();

                    System.Console.Write("Input key string : ");

                    string text = System.Console.ReadLine();
                    ConsoleTheme.Default.Apply();

                    readItem = root.Items.OfType<TextConsoleMenuItem>().FirstOrDefault(i => i.Text.Equals(text, StringComparison.CurrentCultureIgnoreCase));
                    if (readItem != null)
                        return ConsoleUserRequest.SelectItem;
                }
                else if (key.KeyChar == ExitChar)
                    return ConsoleUserRequest.Exit;
                else if (key.Key == ConsoleKey.Backspace)
                {
                    readItem = root.Parent;
                    return ConsoleUserRequest.GoBack;
                }
                else if ((readItem = root.Items.Cast<ConsoleMenuItem>().FirstOrDefault(
                    c => c.KeyCharacter.ToString().Equals(key.KeyChar.ToString(),
                        StringComparison.CurrentCultureIgnoreCase))) != null)
                    return ConsoleUserRequest.SelectItem;
            }
        }

        public static void DoPressEnterToContinue(string message = "Press ENTER to continue...")
        {
            System.Console.WriteLine(message);
            while (true)
            {
                var key = System.Console.ReadKey(true);
                if (key.Key == ConsoleKey.Enter)
                    return;
            }
        }

        public static void WriteInfo(string text)
        {
            ConsoleTheme.Cyan100.Apply();
            System.Console.WriteLine(text);
            ConsoleTheme.Default.Apply();
        }

        public static void WriteError(string text)
        {
            ConsoleTheme.Red1.Apply();
            System.Console.WriteLine(text);
            ConsoleTheme.Default.Apply();
        }
    }
    //TODO: Make a base/abstract console menu that has most of the functionality
    //TODO: Make a console menu that will let you choose objects of a specific type and return it.  Currently we have one for Action. We can add one that handles generics.
}
