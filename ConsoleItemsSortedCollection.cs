﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public class ConsoleMenuItemsCollection : CollectionBase//, IEnumerable<ConsoleMenuItem>
    {
        public ConsoleMenuItem Parent { get; set; }

        public ConsoleMenuItem this[int index]
        {
            get
            {
                return List[index] as ConsoleMenuItem;
            }
        }

        public ConsoleMenuItemsCollection(ConsoleMenuItem parentItem)
        {
            Parent = parentItem;
        }

        public void Add(ConsoleMenuItem item)
        {
            item.Parent = Parent;

            int index = List.Count;
            foreach (var mi in List)
            {
                if (item.CompareTo(mi) < 0)
                {
                    List.Insert(List.IndexOf(mi), item);
                    return;
                }
            }
            List.Add(item);
        }

        public void Remove(int index)
        {
            try
            {
                List.RemoveAt(index);
            }
            catch (ArgumentOutOfRangeException aore)
            {
                throw;
            }
        }

        protected override void OnRemove(int index, object value)
        {
            ((ConsoleMenuItem)List[index]).Parent = null;
            base.OnRemove(index, value);
        }

        internal void Sort()
        {
            
        }
    }
}
