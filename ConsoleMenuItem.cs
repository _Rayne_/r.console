﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public class ConsoleMenuItem : IComparable<ConsoleMenuItem>, IComparable
    {
        public ConsoleMenuItem Parent { get; internal set; }

        public ConsoleMenuItemsCollection Items { get; protected set; }

        public char KeyCharacter { get; set; }
        public string Header { get; set; }
        public string Description { get; set; }
        public ConsoleCommandBase Command { get; set; }

        public ConsoleMenuItem(char keyCharacter, string header, ConsoleCommandBase command, string description = "")
            : this(keyCharacter, header, description)
        {
            Command = command;
        }

        public ConsoleMenuItem(char keyCharacter, string header, string description)
            :this(header, description)
        {
            KeyCharacter = keyCharacter;
        }

        public ConsoleMenuItem(char keyCharacter, string header)
            : this(header)
        {
            KeyCharacter = keyCharacter;
        }

        protected ConsoleMenuItem(string header, string description)
            :this(header)
        {
            Description = description;
        }

        protected ConsoleMenuItem(string header)
        {
            Items = new ConsoleMenuItemsCollection(this);
            Header = header;
        }

        /// <summary>
        /// Selects current menu item and returns true if some comand was executed.
        /// </summary>
        /// <returns>true - command executed, othrwise - false.</returns>
        public bool Select()
        {
            if (Command != null)
            {
                Command.Execute();
                return true;
            }
            return false;
        }

        public virtual void ShowItems()
        {
            //Items.Sort
            Items.Sort();

            foreach (var item in Items)
            {
                System.Console.WriteLine(item.ToString());
            }
        }

        public virtual int CompareTo(ConsoleMenuItem other)
        {
            return KeyCharacter.CompareTo(other.KeyCharacter);
        }

        int IComparable.CompareTo(object obj)
        {
            return CompareTo(obj);
        }

        public virtual int CompareTo(object obj)
        {
            if (!(obj is ConsoleMenuItem))
                throw new ArgumentException("Argument is not a MenuChoice", "obj");
            return this.CompareTo(obj as ConsoleMenuItem);
        }

        public override string ToString()
        {
            return String.Format("  {0} - {1}", KeyCharacter.ToString().ToUpper(), Header);
        }
    }
}
