﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

//This file contains components that are shared throughout the assembly.

namespace R.Console
{
    public enum EscapeKeyBehavior
    {
        DoNothing,
        PressOnceToExit,
        PressTwiceToExit
    }
}