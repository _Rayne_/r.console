﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    class ConsoleTheme
    {
        public static readonly ConsoleTheme Default = new ConsoleTheme(System.Console.ForegroundColor, System.Console.CursorSize);

        public static readonly ConsoleTheme Red1 = new ConsoleTheme(ConsoleColor.Red);

        public static readonly ConsoleTheme Cyan100 = new ConsoleTheme(ConsoleColor.Cyan, 100);

        public ConsoleColor Color { get; private set; }

        public int CursorSize { get; private set; }

        public ConsoleTheme(ConsoleColor color, int cursorSize)
        {
            Color = color;
            if (cursorSize < 1 || cursorSize > 100)
            {
                throw new ArgumentOutOfRangeException("cursorSize", cursorSize, "Console cursor size should be 1 .. 100.");
            }
            CursorSize = cursorSize;
        }

        public ConsoleTheme(ConsoleColor color)
            : this(color, 1)
        {
        }

        internal void Apply()
        {
            System.Console.ForegroundColor = Color;
            System.Console.CursorSize = CursorSize;
        }
    }
}
