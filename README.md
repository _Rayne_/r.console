# R.Console#

R.Console helps you to create simple console menue based on command pattern.

### Simple usage ###

```
#!c#

class FirstComand : ConsoleCommandBase
    {
        public override void Execute()
        {
            Console.Write("Entered to command " + Name + ", input any char ");
            var key = Console.ReadKey(true);
            ConsoleHelper.WriteError("You've entered " + Name + " " + key.KeyChar);
        }
    }

    class SecondComand : ConsoleCommandBase
    {
        public override void Execute()
        {
            ConsoleHelper.WriteError("Command " + Name + " compleated");
        }
    }

    class Program
    {
        static FirstComand cmd1 = new FirstComand() { Name = "FirstTestCommand"};
        static SecondComand cmd2 = new SecondComand() { Name = "SecondTestCommand" };

        static void Main(string[] args)
        {
            var cm = new ConsoleMenu();
            cm.Items.Add(new ConsoleMenuItem('1', "Select char menu item", cmd1));
            cm.Items.Add(new TextConsoleMenuItem("MM", "Select string menu item", cmd2));

            cm.PrependMessage = "Input key char ot press \"Insert\" to input string.";            
            cm.Start();
        }
    }
```