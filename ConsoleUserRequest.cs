﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public enum ConsoleUserRequest
    {
        Exit,
        GoBack,
        SelectItem
    }
}
