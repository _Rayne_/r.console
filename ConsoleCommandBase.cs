﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public abstract class ConsoleCommandBase
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public ConsoleCommandBase()
        {

        }

        public ConsoleCommandBase(string name, string desc)
        {
            Name = name;
            Description = desc;
        }

        public abstract void Execute();
    }
}
