﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace R.Console
{
    public class ConsoleMenu : ConsoleMenuItem
    {
        public string PrependMessage { get; set; }
        //public List<ConsoleMenuItem> Items { get; set; }

        public bool LoopMenuAfterSelection { get; set; }
        public bool ShowPressEnterToContinue { get; set; }

        public ConsoleMenuItem CurrentItem { get; set; }

        public ConsoleMenu(string header)
            : base(String.Empty, header)
        {
            CurrentItem = this;
            LoopMenuAfterSelection = true;
            ShowPressEnterToContinue = true;
            //Items = new List<ConsoleMenuItem>();
        }

        public ConsoleMenu(string prependMessage, string header):
            this(header)
        {
            PrependMessage = prependMessage;
        }

        //public ConsoleMenu AddMenuChoice(Action action, string description)
        //{
        //    int keyCharVal;
        //    int i;
        //    if (Items.Any(mc => int.TryParse(mc.KeyCharacter.ToString(), out i)))
        //    {
        //        keyCharVal = Items.Where(mc => int.TryParse(mc.KeyCharacter.ToString(), out i))
        //                                .Max(mc => int.Parse(mc.KeyCharacter.ToString())) + 1;
        //    }
        //    else
        //    {
        //        keyCharVal = 1;
        //    }

        //    return AddMenuChoice(action, description, char.Parse(keyCharVal.ToString()));
        //}

        //public ConsoleMenu AddMenuChoice(Action action, string description, char keyCharacter)
        //{
        //    if (Items.Any(mc => mc.KeyCharacter.ToString().ToUpper() == keyCharacter.ToString().ToUpper()))
        //        throw new ArgumentException("The keyCharacter specificed has already been assigned: " + keyCharacter.ToString());
        //    if (keyCharacter.ToString().ToUpper() == this.ConsoleMenuOptions.EscapeCharacterMenuOption.ToString().ToUpper())
        //        throw new ArgumentException("The keyCharacter specified is being used as the escape character menu option");
        //    this.Items.Add(new ConsoleMenuItem(action, description, char.Parse(keyCharacter.ToString().ToUpper())));
        //    return this;
        //}

        public void Show()
        {
            if (PrependMessage != null)
            {
                ConsoleHelper.WriteInfo(PrependMessage);
            }

            ConsoleHelper.WriteInfo(CurrentItem.Description);
            System.Console.WriteLine();

            CurrentItem.ShowItems();

            if (CurrentItem.Parent != null)
                System.Console.WriteLine("  {0} - {1}", ConsoleKey.Backspace.ToString(),
                    "Back");

            System.Console.WriteLine("  {0} - {1}", ConsoleHelper.ExitChar.ToString().ToUpper(),
                "Exit");
            System.Console.WriteLine();
        }

        /// <summary>
        /// Returns selected item or null if exit requested.
        /// </summary>
        /// <returns>Selected item or null.</returns>
        public ConsoleMenuItem GetSelectedItem()
        {
            ConsoleMenuItem item = null;

            switch (ConsoleHelper.TryReadItem(CurrentItem,
                out item))
            {
                case ConsoleUserRequest.Exit:
                    return null;
                case ConsoleUserRequest.GoBack:
                    if (item == null)
                        return CurrentItem;    
                    break;
                case ConsoleUserRequest.SelectItem:
                    if (item.Select() && ShowPressEnterToContinue)
                        ConsoleHelper.DoPressEnterToContinue();
                        
                    break;
                default:
                    break;
            }
            
            System.Console.WriteLine();
            return item;
        }

        public void Start()
        {
            while (true)
            {
                /* show menu */
                Show();

                /* get selected item and process actions if any */
                ConsoleMenuItem selected = GetSelectedItem();
                if (null == selected)
                    return;
                else if (selected.Items.Count > 0)
                    CurrentItem = selected;

                System.Console.Clear();

                if (!LoopMenuAfterSelection)
                    return;
            }
        }
    }
}
